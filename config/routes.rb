Rails.application.routes.draw do
  resources :addresses
  root 'pages#home'

  resources :shape_calculators
  resources :sentence_generators
  resources :guess_the_numbers
  resources :users

  get '/login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
end
