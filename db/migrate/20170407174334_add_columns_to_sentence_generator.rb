class AddColumnsToSentenceGenerator < ActiveRecord::Migration[5.0]
  def change
    add_column :sentence_generators, :user_id, :integer
    add_column :sentence_generators, :sentence, :string
  end
end
