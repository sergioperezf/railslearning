class CreateShapeCalculators < ActiveRecord::Migration[5.0]
  def change
    create_table :shape_calculators do |t|
      t.string :shape
      t.string :area
      t.integer :user_id

      t.timestamps
    end
  end
end
