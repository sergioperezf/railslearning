class CreateGuessTheNumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :guess_the_numbers do |t|
      t.integer :tries
      t.integer :number
      t.integer :user_id

      t.timestamps
    end
  end
end
