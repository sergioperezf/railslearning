class SentenceGenerator < ApplicationRecord

  def initialize
    super
    self.sentence = sentences
  end

  # Generates the sentence randomly.
  def sentences
    first_parts = [
        'the pretty cow',
        'we, the people',
        'my auntie Irma',
        'the youth of the Nation',
        'Gabriel',
        'twenty-five bearded men',
        'the cat and the dog',
        'the hole in my bottom'
    ]

    second_parts = [
        'kill the monster with a knife',
        'cooks steak on the iron',
        'open the door with the wrong keys',
        'eats a tuna sandwich for breakfast',
        'answer the phone in a very high pitched voice',
        'learns ruby by reading while in the toilet',
        'drink water from the fountain'
    ]

    first_parts.sample + ' ' + second_parts.sample
  end
end
