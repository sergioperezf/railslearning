class User < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # before_save is a callback that's executed before the record is saved.
  before_save { self.email = email.downcase } # self is not optional in assignments
  # another way to write this is email.downcase! (note the bang, !, meaning that the object mush be modified).

  has_many :guess_the_number

  validates :name, presence: true, length: { maximum: 50 }

  validates :email, presence: true,
                    length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false } # todo make a unique index in db for this field

  validates :password,
                    presence: true,
                    length: {minimum: 8}

  has_secure_password # This is kind of magic, this function adds all the password-related fields, the only
                      # requirement is that a field called password_digest be present in the model, which is
                      # (see related migration).
                      # This also adds the validation for password and password_confirmation virtual fields.
end
