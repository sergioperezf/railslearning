class GuessTheNumber < ApplicationRecord
  belongs_to :user
  validates :tries, numericality: {greater_than: -1},
                    presence: true
  validates :number, presence: true
end
