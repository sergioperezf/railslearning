class ShapeCircle extends React.Component {
    constructor() {
        super();

        this.state = {
            radius: null
        }
    }
    calculateArea() {
        return (this.state.radius * this.state.radius) * 3.141592;
    }

    updateRadius(e) {
        this.setState({radius: e.target.value});
    }

    render() {
        return (
            <div>
                <div className="shape-circle"></div>
                <div>Rectangle</div>
                <div>
                    <label>Radius</label>
                    <input onChange={this.updateRadius.bind(this)} type="number" />
                </div>
            </div>
        );
    }
}