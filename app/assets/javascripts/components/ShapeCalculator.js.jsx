class ShapeCalculator extends React.Component {
    constructor() {
        super();
        this.state = {
            shape: 'rectangle',
            area: null
        };
    }

    calculate() {
        this.setState({area: this.refs.shape.calculateArea()}, () => {
            // setState is async. It does provide this callback function however
            fetch('/shape_calculators.json', {
                credentials: 'include', //pass cookies, for authentication
                method: 'POST',
                headers: {
                    'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                },
                body:   '&shape_calculator[shape]=' + this.state.shape +
                '&shape_calculator[area]=' + this.state.area
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                });
        });
    }

    updateShape(e) {
        this.setState({shape: e.target.value});
    }

    render() {
        switch (this.state.shape) {
            case 'rectangle':
                shape = <ShapeRectangle ref="shape" />;
                break;
            case 'circle':
                shape = <ShapeCircle ref="shape" />;
                break;
            case 'triangle':
                shape = <ShapeTriangle ref="shape" />;
                break;
        }
        return (
            <div>
                <select onChange={this.updateShape.bind(this)}>
                    <option value='rectangle'>Rectangle</option>
                    <option value='circle'>Circle</option>
                    <option value='triangle'>Triangle</option>
                </select>
                {shape}
                <div>
                    <button onClick={this.calculate.bind(this)}>Calculate</button>
                    <div>{this.state.area}</div>
                </div>
            </div>
        );
    }
}