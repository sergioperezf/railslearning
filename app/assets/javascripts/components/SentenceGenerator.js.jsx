class SentenceGenerator extends React.Component {
    constructor() {
        super();
        this.state = {
            sentence: null
        };
    }

    /**
     * Gets the sentence from the backend
     */
    getSentence() {
        fetch('/sentence_generators.json', {
            credentials: 'include',
            method: 'POST',
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    sentence: data.sentence.sentence
                });
            });
    }
    render() {
        return (
            <div>
                <button onClick={this.getSentence.bind(this)}>Generate sentence!</button>
                <h3>{this.state.sentence}</h3>
            </div>
        );
    }
}