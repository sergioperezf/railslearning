class ContactBook extends React.Component {

    constructor() {
        super();
        this.state = {
            contacts: []
        }
    }

    createContact(name, telephone) {
        fetch('/addresses', {
            credentials: 'include',
            method: 'POST',
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            body: '&address[name]=' + name + '&address[telephone]=' + telephone
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    contacts: data.addresses
                })
            });   
    }
    
    componentWillMount() {
        console.log('will mounht');
        fetch('/addresses', {
            credentials: 'include',
            method: 'GET',
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            }
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    contacts: data.addresses
                })
            });
    }

    render() {
        let thead = (
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Telephone</th>
                </tr>
            </thead>
        )
        let contacts = this.state.contacts.map((contact) => {
            return <tr key={contact.id}>
                <td>{contact.name}</td>
                <td>{contact.telephone}</td>
            </tr>
        });
        let tbody = (
            <tbody>
                {contacts}
                <NewContact createContact={this.createContact.bind(this)}/>
            </tbody>
        )
        return (
            <div>
                <table className="table">
                    {thead}
                    {tbody}
                </table>
            </div>
        )
    }
}