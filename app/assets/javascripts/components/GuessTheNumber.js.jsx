/**
 * Guess the name component.
 */
class GuessTheNumber extends React.Component {
    constructor() {
        super();
        this.state = {
            id: null,
            noticeMessage: ''
        };
        this.token = $('meta[name=csrf-token]').attr('content')
        console.log('token: ', this.token)

    }

    /**
     * Creates a new game
     */
    createGame() {
        fetch('/guess_the_numbers.json?&authenticity_token=' + this.token, {
            credentials: 'include', //pass cookies, for authentication
            method: 'POST',
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            body: ''
        })
            .then(response => response.json())
            .then(data => {
                this.setState({id: data.id});
                console.log(data);
            })
    }

    /**
     * Triggers the guessing of the number
     */
    guess() {
        fetch('/guess_the_numbers/' + this.state.id + '.json?&authenticity_token=' + this.token, {
            credentials: 'include', //pass cookies, for authentication
            method: 'PATCH',
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            body: '&guess_the_number[number]=' + this.interface.state.number
        })
            .then(response => response.json())
            .then(data => {
                switch (data.status) {
                    case 'lower':
                        this.setState({
                            noticeMessage: 'The number is lower'
                        });
                        break;
                    case 'higher':
                        this.setState({
                            noticeMessage: 'The number is higher'
                        });
                        break;
                    case 'ok':
                        this.setState({
                            noticeMessage: 'The number is right!',
                            id: null
                        });
                        break;
                }
            })
    }

    render() {

        return (
            <div>
                <GuessTheNumberInterface
                    ref={node => this.interface = node}
                    guessNumber={this.guess.bind(this)}
                    id={this.state.id}
                    createGame={this.createGame.bind(this)}
                    noticeMessage={this.state.noticeMessage}
                />
            </div>
        )
    }
}
