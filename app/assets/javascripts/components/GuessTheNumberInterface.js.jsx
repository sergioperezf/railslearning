/**
 * Interface for the Guess the number game
 */
class GuessTheNumberInterface extends React.Component {
    constructor() {
        super();
        this.state = {
            number: null
        }
    }

    /**
     * Updates state with current number
     * @param e
     */
    updateNumber(e) {
        this.setState({
            number: e.target.value
        })
    }

    sendOnEnter(e) {
        if (e.which === 13) {
            this.props.guessNumber();
        }
    }
    render() {
        let interface = '';
        if (this.props.id) {
            interface = <div>
                <label htmlFor="guess-the-number-input">Type a number</label>
                <input className="form-control" onChange={this.updateNumber.bind(this)} type="number" id="guess-the-number-input" onKeyDown={this.sendOnEnter.bind(this)} />
                <button onClick={this.props.guessNumber}>Guess!</button>
            </div>;
        } else {
            interface = <button onClick={this.props.createGame}>New game</button>
        }

        return (
            <div>
                <div>{this.props.noticeMessage}</div>
                {interface}
            </div>
        );
    }
}