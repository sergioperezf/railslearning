class ShapeRectangle extends React.Component {
    constructor() {
        super();
        this.state = {
            width: null,
            height: null
        };
    }

    calculateArea() {
        return this.state.width * this.state.height;
    }

    updateHeight(e) {
        this.setState({height: e.target.value});
    }

    updateWidth(e) {
        this.setState({width: e.target.value});
    }

    render() {
        return (
            <div>
                <div className="shape-rectangle"></div>
                <div>Rectangle</div>
                <div>
                    <label>Height</label>
                    <input onChange={this.updateHeight.bind(this)} type="number" />
                    <label>Width</label>
                    <input onChange={this.updateWidth.bind(this)} type="number" />
                </div>
            </div>
        );
    }
}