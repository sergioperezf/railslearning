class NewContact extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            telephone: ''
        }
    }

    saveContact(e) {
        if (e.which === 13) {
            this.props.createContact(this.state.name, this.state.telephone);
        } else {
            this.setState({
                name: this.refs.name.value,
                telephone: this.refs.telephone.value
            });
        }
    }

    render() {
        return (
            <tr>
                <td><input 
                    ref='name'
                    placeholder="Name" 
                    className="form-control" 
                    onKeyUp={this.saveContact.bind(this)} />
                </td>
                <td><input 
                    ref='telephone'
                    placeholder="Telephone" 
                    className="form-control"  
                    onKeyUp={this.saveContact.bind(this)} />
                </td>

            </tr>
        );
    }
}