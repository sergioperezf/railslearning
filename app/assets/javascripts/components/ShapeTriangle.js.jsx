class ShapeTriangle extends React.Component {
    calculateArea() {
        return (this.state.width * this.state.height) / 2;
    }

    updateHeight(e) {
        this.setState({height: e.target.value});
    }

    updateWidth(e) {
        this.setState({width: e.target.value});
    }

    render() {
        return (
            <div>
                <div className="shape-triangle"></div>
                <div>Triangle</div>
                <div>
                    <label>Height</label>
                    <input onChange={this.updateHeight.bind(this)} type="number" />
                    <label>Width</label>
                    <input onChange={this.updateWidth.bind(this)} type="number" />
                </div>
            </div>
        );
    }
}