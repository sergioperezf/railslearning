json.array!(@guess_the_numbers) do |guess_the_number|
  json.extract! guess_the_number, :id, :tries, :number, :user_id
  json.url guess_the_number_url(guess_the_number, format: :json)
end
