json.array!(@shape_calculators) do |shape_calculator|
  json.extract! shape_calculator, :id, :shape, :area, :user_id
  json.url shape_calculator_url(shape_calculator, format: :json)
end
