class SentenceGeneratorsController < ApplicationController

  # POST /guess_the_numbers
  # POST /guess_the_numbers.json
  def create
    if !logged_in?
      format.json { render json: {status: 'error'}}
    end
    sentence_generator = SentenceGenerator.new
    sentence_generator.user_id = current_user.id
    if sentence_generator.save
      render json: {status: 'ok', sentence: sentence_generator}
    else
      format.json { render json: {
        status: 'error', 
        message: 'error while saving sentence'
        }}
    end

  end

end
