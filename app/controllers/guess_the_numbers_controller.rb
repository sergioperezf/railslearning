class GuessTheNumbersController < ApplicationController
  before_action :set_guess_the_number, only: [:show, :edit, :update, :destroy]

  # GET /guess_the_numbers
  # GET /guess_the_numbers.json
  def index
    @guess_the_numbers = GuessTheNumber.all
  end

  # GET /guess_the_numbers/1
  # GET /guess_the_numbers/1.json
  def show
  end

  # POST /guess_the_numbers
  # POST /guess_the_numbers.json
  def create
    if !logged_in?
      format.json { render json: {status: 'error'}}
    end
    @guess_the_number = GuessTheNumber.new
    @guess_the_number.tries = 0
    @guess_the_number.user_id = current_user.id
    @guess_the_number.number = rand 1000 # todo this should go in a config variable
    respond_to do |format|
      if @guess_the_number.save
        format.html { redirect_to @guess_the_number, notice: 'Guess the number was successfully created.' }
        format.json { render :show, status: :created, location: @guess_the_number }
      else
        format.html { render :new }
        format.json { render json: @guess_the_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /guess_the_numbers/1
  # PATCH/PUT /guess_the_numbers/1.json
  def update
    respond_to do |format|
      guess_the_number = GuessTheNumber.find params[:id]
      guess_the_number.tries += 1;
      from_db = guess_the_number.number
      from_req = params[:guess_the_number][:number].to_i
      if from_db < from_req
        status = 'higher'
      elsif from_db > from_req
        status = 'lower'
      else
        status = 'ok'
      end

      if guess_the_number.save
        format.html { redirect_to @guess_the_number, notice: 'Guess the number was successfully updated.' }
        format.json { render json: { status: status } }
      else
        format.html { render :edit }
        format.json { render json: @guess_the_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /guess_the_numbers/1
  # DELETE /guess_the_numbers/1.json
  def destroy
    @guess_the_number.destroy
    respond_to do |format|
      format.html { redirect_to guess_the_numbers_url, notice: 'Guess the number was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_guess_the_number
      @guess_the_number = GuessTheNumber.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def guess_the_number_params
      params.require(:guess_the_number).permit(:tries, :number, :user_id)
    end
end
