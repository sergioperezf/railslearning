class AddressesController < ApplicationController
  def create
    if !logged_in?
      format.json { render json: {status: 'error'}}
    else
      @address = Address.new(address_params)
      @address.user_id = current_user.id
      if @address.save
        render json: { status: 'ok', addresses: Address.by_user(current_user)}
      else
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    if !logged_in?
      format.json { render json: {status: 'error'}}
    else
      render json: { status: 'ok', addresses: Address.by_user(current_user)}
    end
  end

  private 
    def address_params
      params.require(:address).permit(:name, :telephone)
    end
end
