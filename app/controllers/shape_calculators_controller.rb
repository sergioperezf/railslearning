class ShapeCalculatorsController < ApplicationController
  before_action :set_shape_calculator, only: [:show, :edit, :update, :destroy]

  # POST /shape_calculators
  # POST /shape_calculators.json
  def create
    if !logged_in?
      format.json { render json: {status: 'error'}}
    end
    @shape_calculator = ShapeCalculator.new(shape_calculator_params)
    @shape_calculator.user_id = current_user.id

    respond_to do |format|
      if @shape_calculator.save
        format.html { redirect_to @shape_calculator, notice: 'Shape calculator was successfully created.' }
        format.json { render :show, status: :created, location: @shape_calculator }
      else
        format.html { render :new }
        format.json { render json: @shape_calculator.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shape_calculator
      @shape_calculator = ShapeCalculator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shape_calculator_params
      params.require(:shape_calculator).permit(:shape, :area)
    end
end
