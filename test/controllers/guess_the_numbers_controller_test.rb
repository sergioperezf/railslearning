require 'test_helper'

class GuessTheNumbersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @guess_the_number = guess_the_numbers(:one)
  end

  test "should get index" do
    get guess_the_numbers_url
    assert_response :success
  end

  test "should get new" do
    get new_guess_the_number_url
    assert_response :success
  end

  test "should show guess_the_number" do
    get guess_the_number_url(@guess_the_number)
    assert_response :success
  end

  test "should get edit" do
    get edit_guess_the_number_url(@guess_the_number)
    assert_response :success
  end

  test "should destroy guess_the_number" do
    assert_difference('GuessTheNumber.count', -1) do
      delete guess_the_number_url(@guess_the_number)
    end

    assert_redirected_to guess_the_numbers_url
  end

  # TODO: should create and should update!
end
