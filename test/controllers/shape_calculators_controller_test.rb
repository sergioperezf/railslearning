require 'test_helper'

class ShapeCalculatorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @shape_calculator = shape_calculators(:one)
  end

  test "should get index" do
    get shape_calculators_url
    assert_response :success
  end

  test "should get new" do
    get new_shape_calculator_url
    assert_response :success
  end

  test "should create shape_calculator" do
    assert_difference('ShapeCalculator.count') do
      post shape_calculators_url, params: { shape_calculator: { area: @shape_calculator.area, shape: @shape_calculator.shape, user_id: @shape_calculator.user_id } }
    end

    assert_redirected_to shape_calculator_url(ShapeCalculator.last)
  end

  test "should show shape_calculator" do
    get shape_calculator_url(@shape_calculator)
    assert_response :success
  end

  test "should get edit" do
    get edit_shape_calculator_url(@shape_calculator)
    assert_response :success
  end

  test "should update shape_calculator" do
    patch shape_calculator_url(@shape_calculator), params: { shape_calculator: { area: @shape_calculator.area, shape: @shape_calculator.shape, user_id: @shape_calculator.user_id } }
    assert_redirected_to shape_calculator_url(@shape_calculator)
  end

  test "should destroy shape_calculator" do
    assert_difference('ShapeCalculator.count', -1) do
      delete shape_calculator_url(@shape_calculator)
    end

    assert_redirected_to shape_calculators_url
  end
end
