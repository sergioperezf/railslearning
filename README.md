# Rails example apps

This is an implementation of Daniweb's [5 Crucial Projects for begginers](https://www.daniweb.com/programming/software-development/threads/131973/5-crucial-projects-for-beginners)
in Rails and ReactJS.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --path vendor/bundle
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```